# 元组排序
my_tuple = (5, 3, 9, 1, 7)
sorted_tuple = sorted(my_tuple)
print(sorted_tuple)

# 字典排序（按键排序）
my_dict = {'c': 3, 'a': 1, 'b': 2}
sorted_dict_keys = sorted(my_dict)
print(sorted_dict_keys)

# 字典排序（按值排序）
sorted_dict_values = sorted(my_dict, key=my_dict.get)
print(sorted_dict_values)