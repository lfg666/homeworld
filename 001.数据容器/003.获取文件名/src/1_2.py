import os

def getFileName(filePath):
    return os.path.splitext(os.path.basename(filePath))[0]