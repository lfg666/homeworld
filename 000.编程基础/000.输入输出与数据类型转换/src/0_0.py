weight = float(input("请输入体重(kg): "))
height, age = map(int, input("请输入身高(cm)年龄: ").split())
activity_level = float(input("输入你的运动系数: "))

BMR = 10 * weight + 6.25 * height - 5 * age + 5
daily_calories = BMR * activity_level

fruit_calories = 50  # 橙子的热量（大卡）
fruit_quantity = daily_calories / fruit_calories

print("每天需要消耗的大卡: {:.3f}".format(daily_calories))
print("满足该热量需求所需食用的水果数量: {:.0f}".format(fruit_quantity))